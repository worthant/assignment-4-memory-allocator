#include "mem.h"
#include "mem_internals.h"
#include <assert.h>
#include <stdio.h>

static void validate_memory_allocation() {
    void* heap = heap_init(0);
    printf("*** Validating Basic Memory Allocation ***\n");
    debug_heap(stdout, heap);
    void* mem = _malloc(0);
    assert(mem != NULL && "Error: Failed to allocate memory");
    debug_heap(stdout, heap);
    printf("Success: Allocated memory at %p\n", mem);
    _free(mem);
    heap_term();
    printf("Success: Memory freed\n\n");
}

static void check_single_block_release() {
    void* heap = heap_init(0);
    printf("*** Checking Release of Single Memory Block ***\n");
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    assert(mem1 != NULL && "Error: Failed to allocate single block");
    debug_heap(stdout, heap);
    _free(mem1);
    heap_term();
    printf("Success: Single block released\n\n");
}

static void verify_multiple_blocks_release() {
    void* heap = heap_init(0);
    printf("*** Verifying Release of Multiple Memory Blocks ***\n");
    debug_heap(stdout, heap);
    void* mem1 = _malloc(64);
    void* mem2 = _malloc(128);
    assert(mem1 != NULL && "Error: Failed to allocate first block");
    assert(mem2 != NULL && "Error: Failed to allocate second block");
    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    heap_term();
    printf("Success: Multiple blocks released\n\n");
}

static void test_heap_extension() {
    void* heap = heap_init(0);
    printf("*** Testing Heap Extension Functionality ***\n");
    debug_heap(stdout, heap);
    void* mem1 = _malloc(4096);
    void* mem2 = _malloc(8129);
    assert(mem1 != NULL && "Error: Failed in initial allocation");
    assert(mem2 != NULL && "Error: Failed in extended allocation");
    debug_heap(stdout, heap);
    _free(mem1);
    _free(mem2);
    heap_term();
    printf("Success: Heap extension and release function as expected\n\n");
}

static void assess_heap_extension_to_another_place() {
    void* heap = heap_init(0);
    printf("*** Assessing Heap Extension with an Obstacle ***\n");
    debug_heap(stdout, heap);
    void* obstacle = _malloc(4096);
    void* mem = _malloc(8192);
    assert(obstacle != NULL && "Error: Failed to allocate obstacle block");
    assert(mem != NULL && "Error: Failed to allocate with obstacle in place");
    debug_heap(stdout, heap);
    _free(obstacle);
    _free(mem);
    heap_term();
    printf("Success: Managed heap extension with obstacle and freed memory\n\n");
}

int main() {
    validate_memory_allocation();
    check_single_block_release();
    verify_multiple_blocks_release();
    test_heap_extension();
    assess_heap_extension_to_another_place();
    return 0;
}